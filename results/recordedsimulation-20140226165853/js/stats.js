var stats = {
	type: "GROUP",
contents: {
		
				"request-1-46da482b5ba7614b7124accf72d8b1ce": {
		type: "REQUEST",
		name: "request_1",
path: "request_1",
pathFormatted: "request-1-46da482b5ba7614b7124accf72d8b1ce",
stats: {
	numberOfRequests : {
		total: "1",
		ok: "1",
		ko: "0"
	},
	minResponseTime : {
		total: "90",
		ok: "90",
		ko: "-"
	},
	maxResponseTime : {
		total: "90",
		ok: "90",
		ko: "-"
	},
	meanResponseTime : {
		total: "90",
		ok: "90",
		ko: "-"
	},
	standardDeviation : {
		total: "0",
		ok: "0",
		ko: "-"
	},
	percentiles1 : {
		total: "90",
		ok: "90",
		ko: "-"
	},
	percentiles2 : {
		total: "90",
		ok: "90",
		ko: "-"
	},
	group1 : {
		name: "t < 800 ms",
		count: 1,
		percentage: 100
	},
	group2 : {
		name: "800 ms < t < 1200 ms",
		count: 0,
		percentage: 0
	},
	group3 : {
		name: "t > 1200 ms",
		count: 0,
		percentage: 0
	},
	group4 : {
		name: "failed",
		count: 0,
		percentage: 0
	},
	meanNumberOfRequestsPerSecond: {
		total: "1",
		ok: "1",
		ko: "-"
	}
}


	}
		,		
				"request-1-redirect-1-7e85b8405f9bd7a607de8ec9187a9faa": {
		type: "REQUEST",
		name: "request_1 Redirect 1",
path: "request_1 Redirect 1",
pathFormatted: "request-1-redirect-1-7e85b8405f9bd7a607de8ec9187a9faa",
stats: {
	numberOfRequests : {
		total: "1",
		ok: "1",
		ko: "0"
	},
	minResponseTime : {
		total: "550",
		ok: "550",
		ko: "-"
	},
	maxResponseTime : {
		total: "550",
		ok: "550",
		ko: "-"
	},
	meanResponseTime : {
		total: "550",
		ok: "550",
		ko: "-"
	},
	standardDeviation : {
		total: "0",
		ok: "0",
		ko: "-"
	},
	percentiles1 : {
		total: "550",
		ok: "550",
		ko: "-"
	},
	percentiles2 : {
		total: "550",
		ok: "550",
		ko: "-"
	},
	group1 : {
		name: "t < 800 ms",
		count: 1,
		percentage: 100
	},
	group2 : {
		name: "800 ms < t < 1200 ms",
		count: 0,
		percentage: 0
	},
	group3 : {
		name: "t > 1200 ms",
		count: 0,
		percentage: 0
	},
	group4 : {
		name: "failed",
		count: 0,
		percentage: 0
	},
	meanNumberOfRequestsPerSecond: {
		total: "1",
		ok: "1",
		ko: "-"
	}
}


	}
		,		
				"request-2-93baff648d9a0c13a48ee1d38e7b220f": {
		type: "REQUEST",
		name: "request_2",
path: "request_2",
pathFormatted: "request-2-93baff648d9a0c13a48ee1d38e7b220f",
stats: {
	numberOfRequests : {
		total: "1",
		ok: "1",
		ko: "0"
	},
	minResponseTime : {
		total: "140",
		ok: "140",
		ko: "-"
	},
	maxResponseTime : {
		total: "140",
		ok: "140",
		ko: "-"
	},
	meanResponseTime : {
		total: "140",
		ok: "140",
		ko: "-"
	},
	standardDeviation : {
		total: "0",
		ok: "0",
		ko: "-"
	},
	percentiles1 : {
		total: "140",
		ok: "140",
		ko: "-"
	},
	percentiles2 : {
		total: "140",
		ok: "140",
		ko: "-"
	},
	group1 : {
		name: "t < 800 ms",
		count: 1,
		percentage: 100
	},
	group2 : {
		name: "800 ms < t < 1200 ms",
		count: 0,
		percentage: 0
	},
	group3 : {
		name: "t > 1200 ms",
		count: 0,
		percentage: 0
	},
	group4 : {
		name: "failed",
		count: 0,
		percentage: 0
	},
	meanNumberOfRequestsPerSecond: {
		total: "1",
		ok: "1",
		ko: "-"
	}
}


	}
		},
name: "Global Information",
path: "",
pathFormatted: "missing-name",
stats: {
	numberOfRequests : {
		total: "3",
		ok: "3",
		ko: "0"
	},
	minResponseTime : {
		total: "90",
		ok: "90",
		ko: "-"
	},
	maxResponseTime : {
		total: "550",
		ok: "550",
		ko: "-"
	},
	meanResponseTime : {
		total: "260",
		ok: "260",
		ko: "-"
	},
	standardDeviation : {
		total: "206",
		ok: "206",
		ko: "-"
	},
	percentiles1 : {
		total: "550",
		ok: "550",
		ko: "-"
	},
	percentiles2 : {
		total: "550",
		ok: "550",
		ko: "-"
	},
	group1 : {
		name: "t < 800 ms",
		count: 3,
		percentage: 100
	},
	group2 : {
		name: "800 ms < t < 1200 ms",
		count: 0,
		percentage: 0
	},
	group3 : {
		name: "t > 1200 ms",
		count: 0,
		percentage: 0
	},
	group4 : {
		name: "failed",
		count: 0,
		percentage: 0
	},
	meanNumberOfRequestsPerSecond: {
		total: "4",
		ok: "4",
		ko: "-"
	}
}



}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
