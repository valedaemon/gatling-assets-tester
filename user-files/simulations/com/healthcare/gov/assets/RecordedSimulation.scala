package com.healthcare.gov.assets 
import com.excilys.ebi.gatling.core.Predef._
import com.excilys.ebi.gatling.http.Predef._
import com.excilys.ebi.gatling.jdbc.Predef._
import com.excilys.ebi.gatling.http.Headers.Names._
import akka.util.duration._
import bootstrap._
import assertions._

class RecordedSimulation extends Simulation {

	val httpConf = httpConfig
			.baseURL("http://www.healthcare.gov")
			.acceptHeader("image/webp,*/*;q=0.8")
			.acceptEncodingHeader("gzip,deflate,sdch")
			.acceptLanguageHeader("en-US,en;q=0.8")
			.connection("keep-alive")
			.userAgentHeader("Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.117 Safari/537.36")


	val headers_1 = Map(
			"Accept" -> """text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8"""
	)


	val scn = scenario("Scenario Name")
		.exec(http("request_1")
					.get("/")
					.headers(headers_1)
			)
		.exec(http("request_2")
					.get("http://assets.healthcare.gov/global/js/lib/jquery-1.8.2.js")
			)

	setUp(scn.users(1).protocolConfig(httpConf))
}